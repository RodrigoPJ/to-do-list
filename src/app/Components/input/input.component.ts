import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

@Component({
  selector: "app-input",
  templateUrl: "./input.component.html",
  styleUrls: ["./input.component.css"]
})
export class InputComponent implements OnInit {
  @Output() activity = new EventEmitter<string>();

  myInputForm: FormGroup;
  constructor(private formBuilder: FormBuilder) {}

  ngOnInit() {
    this.myInputForm = this.formBuilder.group({
      input: ["", Validators.required]
    });
  }

  addAction() {
    if (this.myInputForm.invalid) return;
    this.activity.emit(this.myInputForm.controls.input.value);
    this.myInputForm.controls.input.reset();
  }
}
