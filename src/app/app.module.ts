import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule} from "@angular/forms";

import { DataService } from "./Data/data.service";

import { AppComponent } from "./app.component";
import { InputComponent } from "./Components/input/input.component";
import { ListComponent } from "./Components/list/list.component";
import { ContainerComponent } from "./Components/container/container.component";
import { FromNowPipe } from './from-now.pipe';
import { ModalHostDirective } from './Directives/modal-host.directive';
import { ModalComponent } from './Components/modal/modal.component';

@NgModule({
  declarations: [
    AppComponent,
    InputComponent,
    ListComponent,
    ContainerComponent,
    FromNowPipe,
    ModalHostDirective,
    ModalComponent
  ],
  imports: [BrowserModule, FormsModule, ReactiveFormsModule],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule {}
