import { Component, OnInit, Input, Output, EventEmitter} from "@angular/core";
import {Activity} from '../../Data/data.service'

@Component({
  selector: "app-list",
  templateUrl: "./list.component.html",
  styleUrls: ["./list.component.css"]
})
export class ListComponent implements OnInit {
  @Output() delete = new EventEmitter<string>();
  @Input() activityList:Activity[];

  constructor() {}

  ngOnInit() {}

  deleteActivity(itemText:string):void {
    this.delete.emit(itemText);
  }

  public trackByFn(index:Activity):string {
    if (!index) return null;
    return index.text;
  }
}
