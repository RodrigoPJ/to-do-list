import { Component, OnInit } from '@angular/core';
import {DataService, Activity} from '../../Data/data.service';

@Component({
  selector: 'app-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.css']
})
export class ContainerComponent implements OnInit {
  activityList:Activity[] = [];

  constructor(private dataService:DataService) { }

  ngOnInit() {
    this.activityList = this.dataService.getActivityList();
  }
  addActivity(activity:string){
    this.dataService.addActivity(activity);
  }
  deleteActivity(text:string){
    this.dataService.deleteActivity(text);
  }

}
