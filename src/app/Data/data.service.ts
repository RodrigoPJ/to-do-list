import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class DataService {
  activityList: Activity[] = [
    {
      text: "dummy Activity",
      dateCreated: Date.now() - 50000000,
      isCrossed: false
    }
  ];

  constructor() {}

  getActivityList() {
    return this.activityList;
  }
  addActivity(activityText: string) {
    let activity: Activity = {
      text: activityText,
      dateCreated: Date.now(),
      isCrossed: false
    };
    this.activityList.push(activity);
  }
  deleteActivity(text: string) {
    let index = this.activityList.findIndex(item => item.text === text);
    this.activityList.splice(index,1);
  }
}

export interface Activity {
  text: string;
  dateCreated: number;
  isCrossed: boolean;
}
